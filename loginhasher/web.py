import flask
from flask import render_template, request
import os
import sys
import login
import json


basedir = os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__)
app = flask.Flask(__name__, template_folder=os.path.join(basedir, 'templates'), static_folder=os.path.join(basedir, 'static'),)
srv = None


def start(port_):
   app.run(debug=True, use_debugger=False, use_reloader=False) # or ("localhost")


def stop():
   global srv
   if srv:
       srv._do_serve = 0


@app.route("/")
def menu():
   return render_template("index.html")



