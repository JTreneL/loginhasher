import pymongo
import bcrypt
import sys
import web
import threading
import time


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
# DB
mydb = myclient["Login"]
# Collection Users
mycol = mydb["Users"]


# Verify, password == hashed and encoded password in db
def verify(password,hashedpassdb):
    if password == hashedpassdb:
        return True
    else:
        return False


def hashing(a,salt):
    hasher = a.encode()
    hashed = bcrypt.hashpw(hasher,salt)
    hashed = hashed.decode('utf-8')
    return hashed


# Register for form > Variables > generate salt > hashing password with salt and decode > salt dedoce > insert into mongo db:
def register(getnick,getpass):
    # Generate salt
    salt = bcrypt.gensalt()
    getpass = hashing(getpass,salt)
    userinsert = {"Nick": getnick, "Password": getpass, "Salt": salt}
    x = mycol.insert_one(userinsert)
    varregistered = "Registered"
    return varregistered


# Statements missing. 
def login(getnicklog,passwordlog):
    # Nick to DB > 
    getnick = {"Nick":getnicklog} # Working
    # Fetch Nick/Pass,Salt
    usernick = mycol.find_one(getnick)["Nick"] # Working
    userpass = mycol.find_one(getnick)["Password"] # Working
    usersalt = mycol.find_one(getnick)["Salt"] # Working
    passwordlog = hashing(passwordlog,usersalt)
    check = verify(passwordlog, userpass)
    if check == True:
        # continue to logged html
        print("True")
    else:
        # bad password return to login
        print("False")


def main():
    port = 5000
    run_web = threading.Thread(target=web.start, args=(port,))
    run_web.start()
    while True:
            try:
                time.sleep(1000)
            except KeyboardInterrupt: 
                break
    web.stop()
    

if __name__ == "__main__":
    sys.exit(main())





